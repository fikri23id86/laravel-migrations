@extends('template.master')

@section('content')
<div class="card p-4">
  <div class="mb-4 text-center">
    <h1 class="mb-4">{{ $item->judul }}</h1>
  </div>
  <p>{{ $item->isi }}</p>
  <div class="text-muted mt-4">
    <small>Updated at : {{ $item->tanggal_diperbaharui }}</small>
    <br>
    <small>Created at : {{ $item->tanggal_dibuat }}</small>
  </div>
</div>
@endsection